#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "maths.h"
#include <iostream>
#include "maths.cpp"


bool bool_prime(int n){
  bool boolPrime = true;

  if(n == 0 || n == 1)
    {
      boolPrime = false;
    }
  else {
    for(int i = 2; i<=n/2; i++){
      if(n % i == 0){
	boolPrime = false;
	break;
	
      }
    }
  }
  return boolPrime;
}

TEST_CASE("test primes", "[bool_prime]")
{
  REQUIRE(is_prime(2) == true);
  REQUIRE(is_prime(3) == true);
  REQUIRE(is_prime(4) == false);
  REQUIRE(is_prime(5) == true);
  REQUIRE(is_prime(6) == false);
  REQUIRE(is_prime(7) == true);
  REQUIRE(is_prime(8) == false);
  REQUIRE(is_prime(9) == false);
}
